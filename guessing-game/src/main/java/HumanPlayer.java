class HumanPlayer implements Player {

    private final UI ui;

    HumanPlayer(UI ui) {
        this.ui = ui;
    }

    @Override
    public int getNumber(Range range) {
        int number;
        do {
            number = ui.readInt(String.format("Enter a number within %s:", range));
        } while (!range.in(number));
        return number;
    }

}
