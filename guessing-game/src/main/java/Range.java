class Range {

    /*@
      @ ensures \result == (0 <= lb && lb <= ub && ub < Integer.MAX_VALUE);
      @ model static pure helper function boolean validRange(int lb, int ub);
      @*/

    final int lowerBound;
    final int upperBound;

    /*@
      @ invariant validRange(lowerBound, upperBound);
      @*/

    /*@
      @ requires validRange(lowerBound, upperBound);
      @*/
    Range(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    /*@
      @ ensures \result <==> (lowerBound <= number && number <= upperBound);
      @*/
    /*@ pure @*/ boolean in(int number) {
        return lowerBound <= number && number <= upperBound;
    }

    @Override
    public String toString() {
        return String.format("<%d, %d>", lowerBound, upperBound);
    }

}
