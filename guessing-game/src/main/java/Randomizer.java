import java.security.SecureRandom;
import java.util.Random;

class Randomizer implements Player {

    private final Random random;

    Randomizer() {
        random = new SecureRandom();
    }

    @Override
    public int getNumber(Range range) {
        return random.nextInt(range.upperBound - range.lowerBound + 1) + range.lowerBound;
    }

}
