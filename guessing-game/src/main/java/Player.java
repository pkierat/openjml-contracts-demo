interface Player {

    //@ ensures range.in(\result);
    int getNumber(Range range);

}
