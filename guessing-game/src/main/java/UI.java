import java.util.Scanner;

public interface UI {

    int readInt(String message);

    void display(String message);

    class Console implements UI {

        private final Scanner scanner;

        Console() {
            scanner = new Scanner(System.in);
        }

        @Override
        public int readInt(String message) {
            do {
                display(message);
                String value = scanner.nextLine();
                try {
                    return Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    displayError("Wrong number format!");
                }
            } while (true);
        }

        @Override
        public void display(String message) {
            System.out.println(message);
        }

        private void displayError(String message) {
            System.err.println(message);
        }

    }
}
