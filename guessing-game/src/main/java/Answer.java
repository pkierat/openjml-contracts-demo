interface Answer {

    //@ public model instance boolean correct;

    //@ ensures \result == correct;
    /*@ pure @*/ boolean isCorrect();

}
