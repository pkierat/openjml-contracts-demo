class Main {

    public static void main(String[] args) {
        new GuessingGameImpl(
                new Range(1, 100),
                new UI.Console())
                .play();
    }

}
