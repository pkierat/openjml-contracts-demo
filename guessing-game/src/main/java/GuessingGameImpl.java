class GuessingGameImpl implements GuessingGame {

    private final Range range;

    private final UI ui;

    private final Player computer;
    private final Player human;

    GuessingGameImpl(Range range, UI ui) {
        this.range = range;
        this.ui = ui;
        computer = new Randomizer();
        human = new HumanPlayer(ui);
    }

    @Override
    public void play() {
        int generatedNumber = computer.getNumber(range);
        Answer answer;
        do {
            int givenNumber = human.getNumber(range);
            answer = compare(generatedNumber, givenNumber);
            provideFeedback(answer);
        } while (!answer.isCorrect());
    }

    /*@
      @ requires range.in(generatedNumber);
      @ requires range.in(givenNumber);
      @ ensures \result.correct <==> (generatedNumber == givenNumber);
      @*/
    private Answer compare(int generatedNumber, int givenNumber) {
        return generatedNumber == givenNumber ? new Correct(generatedNumber, givenNumber)
                : generatedNumber < givenNumber ? new TooHigh(generatedNumber, givenNumber)
                : new TooLow(generatedNumber, givenNumber);
    }

    private void provideFeedback(Answer answer) {
        ui.display(String.format("Your number is %s!", answer));
    }

    private abstract static class AbstractAnswer implements Answer {

        private final boolean equal; //@ in correct;
        //@ private represents correct = equal;

        //@ ensures correct <==> (generatedNumber == givenNumber);
        private AbstractAnswer(int generatedNumber, int givenNumber) {
            equal = (generatedNumber == givenNumber);
        }

        @Override
        public boolean isCorrect() {
            return equal;
        }

    }

    private static class Correct extends AbstractAnswer {

        //@ requires generatedNumber == givenNumber;
        //@ ensures correct;
        private Correct(int generatedNumber, int givenNumber) {
            super(generatedNumber, givenNumber);
        }

        @Override
        public String toString() {
            return "correct";
        }

    }

    private static class TooHigh extends AbstractAnswer {

        //@ requires generatedNumber < givenNumber;
        //@ ensures !correct;
        private TooHigh(int generatedNumber, int givenNumber) {
            super(generatedNumber, givenNumber);
        }

        @Override
        public String toString() {
            return "too high";
        }

    }

    private static class TooLow extends AbstractAnswer {

        //@ requires generatedNumber > givenNumber;
        //@ ensures !correct;
        private TooLow(int generatedNumber, int givenNumber) {
            super(generatedNumber, givenNumber);
        }

        @Override
        public String toString() {
            return "too low";
        }

    }

}
