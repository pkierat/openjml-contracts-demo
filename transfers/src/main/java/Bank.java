import java.util.LinkedList;
import java.util.List;

class Bank {

    private final List<Transaction> log =  new LinkedList<>();

    /*@
      @ requires src != dst;
      @ requires amount >= 0;
      @ ensures !\result ==> \not_modified(src.balance) && \not_modified(dst.balance);
      @ ensures \result ==> src.balance == \old(src.balance) - amount;
      @ ensures \result ==> dst.balance == \old(dst.balance) + amount;
      @*/
    boolean transfer(Account src, Account dst, int amount) {
        try {
            src.withdraw(amount);
            dst.deposit(amount);
            log.add(Transaction.success(src, dst, amount));
            return true;
        } catch (NotEnoughMoneyException e) {
            log.add(Transaction.failure(src, dst, amount, e));
        } catch (TooMuchMoneyException e) {
            try {
                src.deposit(amount);
            } catch (TooMuchMoneyException ex) {
                // ignore
            } finally {
                log.add(Transaction.failure(src, dst, amount, e));
            }
        }
        return false;
    }

}
