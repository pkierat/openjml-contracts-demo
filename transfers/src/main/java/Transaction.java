final class Transaction {

    private final Account source;
    private final Account destination;
    private final int amount;
    private final State state;
    /*@ nullable @*/
    private final Exception cause;

    /*@ pure @*/ private Transaction(Account source,
                                     Account destination,
                                     int amount,
                                     State state,
                                     /*@ nullable @*/ Exception cause) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
        this.state = state;
        this.cause = cause;
    }

    /*@ pure @*/
    static Transaction success(Account src, Account dst, int amount) {
        return new Transaction(src, dst, amount, State.SUCCESS, null);
    }

    /*@ pure @*/
    static Transaction failure(Account src, Account dst, int amount, /*@ nullable @*/ Exception cause) {
        return new Transaction(src, dst, amount, State.FAILURE, cause);
    }

    enum State {
        SUCCESS,
        FAILURE
    }

    public Account getSource() {
        return source;
    }

    public Account getDestination() {
        return destination;
    }

    public int getAmount() {
        return amount;
    }

    public State getState() {
        return state;
    }

    /*@ nullable @*/
    public Exception getCause() {
        return cause;
    }

}
