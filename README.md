# Contracts in Java with OpenJML - Demo

## Description

This repository contains a code that demonstrates, how one can use
Java Modeling Language (JML) to define contracts between various components
and methods within the application.

## Usage

1. Download [OpenJML](https://openjml.org)
1. Unpack to a folder of your choice
1. Clone this repository
1. Adjust `openjml` script to reflect your OpenJML installation
1. Integrate the script with your IDE
   * Intellij IDEA, sample instructions are written inside the script
1. Try to perform the analysis
1. Browse the history to see the changes made step by step
   * Each commit was made when OpenJML returned no warnings
   * Commit messages contain a detailed description of the steps

