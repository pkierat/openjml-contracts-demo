# Contracts in Java with OpenJML

## Contract

### Example 1: Bean Validation

```java
class Customer {

    @NotNull
    private String name;

    @Valid
    private Address address;

    @Pattern("\\+[0-9]{11,12}")
    private String mobilePhoneNumber;

    [...]
}
```

### Example 2: Spring Cloud

```yaml
request:
  method: PUT
  url: /fraudcheck
  body:
    "client.id": 1234567890
    loanAmount: 99999
  headers:
    Content-Type: application/json
  matchers:
    body:
      - path: $.['client.id']
        type: by_regex
        value: "[0-9]{10}"
response:
  status: 200
  body:
    fraudCheckStatus: "FRAUD"
    "rejection.reason": "Amount too high"
  headers:
    Content-Type: application/json;charset=UTF-8
```

### Example 3: Equals

```java
/**
 * [...]
 * <li>It is <i>reflexive</i>: for any non-null reference 
 *     value {@code x}, {@code x.equals(x)} should return
 *     {@code true}.
 * <li>It is <i>symmetric</i>: for any non-null reference 
 *     values {@code x} and {@code y}, {@code x.equals(y)}
 *     should return {@code true} if and only if
 *     {@code y.equals(x)} returns {@code true}.
 * <li>It is <i>transitive</i>: for any non-null reference 
 *     values {@code x}, {@code y}, and {@code z}, if
 *     {@code x.equals(y)} returns {@code true} and
 *     {@code y.equals(z)} returns {@code true}, then
 *     {@code x.equals(z)} should return {@code true}.
 * [...]
 */
public boolean equals(Object obj) { ... }
```

* Reflexivity
  ```math
  \forall_{x}{x.equals(x)}
  ```

* Symmetry
  ```math
  \forall_{x, y} x.equals(y) \iff y.equals(x)
  ```

* Transitivity
  ```math
  \forall_{x,y,z} x.equals(y) \wedge y.equals(z) \Rightarrow x.equals(z)
  ```

## Java Modeling Language (JML)

* Interface **specification** language for Java

* Formal assertions about the code

* Written (mostly) as Java comments

* Implementation: OpenJML ([www.openjml.org](https://openjml.org))

  * Java Compiler extension (based on OpenJDK)

  * **Proves correctness** at compile-time

  * **Verifies assertions** at run-time

### What is *Specification*?

* Preconditions
  ```java
  //@ requires <precondition>;
  ```

* Postconditions
  ```java
  //@ ensures <postcondition>;
  ```

* Exceptional postconditions
  ```java
  //@ signals (<exception-type>) <condition>;
  ```

* Invariants
  ```java
  //@ invariant <condition>;
  ```

* More...

## Benefits

* Focus on functional requirements

  * Forces to think about pre- and postconditions

* Detects more errors than testing and SCA

  * Based on reasoning, not on examples

* Makes the code easier to test

  * Tests can be generated automatically

* Makes the code more *Clean* and *S.O.L.I.D.*

## Drawbacks and Pitfalls

* Writing useful assertions is difficult

  * Requires some practice

* Code is as good as the specification

  * Correctness is a relative term

* Neither sound, nor complete

  * Possible false positives and negatives

## References

* Books
  * Bertrand Meyer: *Object-Oriented Software Construction, 2nd Ed.*, Prentice Hall, 1997.
* Links
  * <http://jmlspecs.org>
  * <http://openjml.org>

